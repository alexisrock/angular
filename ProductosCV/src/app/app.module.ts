import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest} from '@angular/common/http';
import { AppComponent } from './app.component';
import { ProductoprouebaComponent } from './Components/productoproueba/productoproueba.component';
import { ListaproductosComponent } from './Components/listaproductos/listaproductos.component';
import { ProductoDetalleComponent } from './Components/producto-detalle/producto-detalle.component';
import { FormProductoComponent } from './Components/form-producto/form-producto.component';
import { Productoapicliente } from './models/Productoapicliente.model';
import { Producto } from './models/Producto.model';
import  Dexie  from 'dexie';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import {  TranslateLoader, TranslateService } from '@ngx-translate/core'
import { InitMyDataAction, intializeProducto, ProductoEffects, productostate, ReducerProductos } from './models/productostate.model';
import { ActionReducerMap, Store } from '@ngrx/store';
import {  StoreModule } from '@ngrx/store';
import {  EffectsModule } from '@ngrx/effects';
import {  StoreDevtoolsModule } from '@ngrx/store-devtools';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './Components/login/login/login.component';
import { ProtectedComponent } from './Components/protected/protected/protected.component';  
import { UsuarioLogueadoGuard } from './guard/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponentComponent } from './Components/vuelos/vuelos-component/vuelos-component.component';
import { VueloMainComponentComponent } from './Components/vuelos/vuelo-main-component/vuelo-main-component.component';
import { VueloMasInfoComponentComponent } from './Components/vuelos/vuelo-mas-info-component/vuelo-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './Components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasListadoComponent } from './reservas/reservas-listado/reservas-listado.component';
import { ReservasModule } from './reservas/reservas.module';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl/lib/ngx-mapbox-gl.module';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

export interface AppConfig{
    apiEndpoint: string;
  };

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: "http://localhost:3000"
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VueloMainComponentComponent },
  { path: 'mas-info', component: VueloMasInfoComponentComponent },
  { path: ':id', component: VuelosDetalleComponentComponent },
];

// app init
export function init_app(appLoadService: AppLoadService): () => Promise<any>  {
  return () => appLoadService.intializeDestinosViajesState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  async intializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}

// fin app init

export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}



@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  productos: Dexie.Table<Producto, number>;
  translations: Dexie.Table<Translation, number>;
  constructor () {
      super('MyDatabase');    
      this.version(2).stores({
        productos: '++id, producto, imagenUrl',    
      });

      this.version(2).stores({
        productos: '++id, producto, imagenUrl',
        translations: '++id, lang, key, value'
      });
  }
}

class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) { }

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results => {
                                        if (results.length === 0) {
                                          return this.http
                                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                                            .toPromise()
                                            .then(apiResults => {
                                              db.translations.bulkAdd(apiResults);
                                              return apiResults;
                                            });
                                        }
                                        return results;
                                      }).then((traducciones) => {
                                        console.log('traducciones cargadas:');
                                        console.log(traducciones);
                                        return traducciones;
                                      }).then((traducciones) => {
                                        return traducciones.map((t) => ({ [t.key]: t.value}));
                                      });
    /*
    return from(promise).pipe(
      map((traducciones) => traducciones.map((t) => { [t.key]: t.value}))
    );
    */
   return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}





export const db = new MyDatabase();

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home',  component: ListaproductosComponent },
  { path: 'producto/:id',  component: ProductoDetalleComponent },
  { path: 'login',  component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [ UsuarioLogueadoGuard ]
  },
  {
    path: 'vuelos',
    component: VuelosComponentComponent,
    canActivate: [ UsuarioLogueadoGuard ],
    children: childrenRoutesVuelos
  },
];

//redux init
export interface AppState{
 producto: productostate;
} 

const reducers: ActionReducerMap<AppState>={
  producto: ReducerProductos
}

let reducersInitialstate = {
  producto: intializeProducto()
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}
@NgModule({
  declarations: [
    AppComponent,
    ProductoprouebaComponent,
    ListaproductosComponent,
    ProductoDetalleComponent,
    FormProductoComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VueloMainComponentComponent,
    VueloMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    ReservasListadoComponent,
    EspiameDirective,
    TrackearClickDirective
  
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    StoreModule.forRoot(reducers, {initialState: reducersInitialstate}),
    EffectsModule.forRoot([ProductoEffects]),
    StoreDevtoolsModule.instrument(),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (HttpLoaderFactory),
          deps: [HttpClient]
      }
    }),
    ReservasModule,  
    NgxMapboxGLModule,
    BrowserAnimationsModule

  ],
  providers: [  
    Productoapicliente ,  
    UsuarioLogueadoGuard,
    AuthService,
    {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    AppLoadService,
    MyDatabase,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
