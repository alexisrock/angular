import { forwardRef, Inject, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Subject } from 'rxjs';
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module';
import { Producto } from './Producto.model';
import { ElegidoFavoritoAction, NuevoProductoAction } from './productostate.model';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
@Injectable()
export class Productoapicliente {
  producto: Producto[] = [];
  constructor(public store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient
    ) {

      this.store
      .select(state => state.producto)
      .subscribe((data) => {
        console.log('destinos sub store');
        console.log(data);
        this.producto = data.items;
      });
    this.store
      .subscribe((data) => {
        console.log('all store');
        console.log(data);
      });  

  }


  add(P: Producto) {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: P.producto }, { headers: headers });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoProductoAction(P));
        const myDb = db;
        myDb.productos.add(P);
        console.log('todos los destinos de la db!');
        myDb.productos.toArray().then(product => console.log(product))
      }
    });   
  }




  elegir(p: Producto){
    this.store.dispatch(new ElegidoFavoritoAction(p));
  }
  getAll(): Producto[] {
    return this.producto;
  }

  getById(id: String): Producto {
    return this.producto.filter(function(d) { return d.producto.toString() === id; })[0];
  }

}