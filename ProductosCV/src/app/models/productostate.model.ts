import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Producto } from './Producto.model';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';

//Estado
export interface productostate{
    items: Producto[];
    loading: boolean;
    favorito: Producto;

}

export function  intializeProducto(){
    return {
        items: [],
        loading: false,
        favorito: null
    };
};

//Acciones
export enum ProductoActiontype{
    Nuevo_Producto = '[Producto] Nuevo',
    Elegido_favorito = '[Producto] Favorito',
    Vote_up = '[Producto] Vote Up',
    Vote_down = '[Producto] Vote Down',
    INIT_MY_DATA = '[Producto] Init My Data'
}

export class NuevoProductoAction implements Action{
    type= ProductoActiontype.Nuevo_Producto;
    constructor(public producto: Producto){ }

}

export class ElegidoFavoritoAction implements Action{
    type= ProductoActiontype.Elegido_favorito;
    constructor(public producto: Producto){ }

}


export class VoteupAction implements Action{
    type= ProductoActiontype.Vote_up;
    constructor(public producto: Producto){ }

}

export class VoteDownAction implements Action{
    type= ProductoActiontype.Vote_down;
    constructor(public producto: Producto){ }

}

export class InitMyDataAction implements Action {
    type = ProductoActiontype.INIT_MY_DATA;
    constructor(public productos: string[]) {}
  }

export type ProductoActions = NuevoProductoAction | ElegidoFavoritoAction
|VoteupAction | VoteDownAction  | InitMyDataAction;

//Reducers

export function ReducerProductos(
    state: productostate,
    action: ProductoActions
):productostate{
 switch(action.type)
    {
        case ProductoActiontype.Nuevo_Producto: {
            return {
                ...state,
                items: [...state.items, (action as NuevoProductoAction).producto]
            };
        }
        case ProductoActiontype.Elegido_favorito: {
            let productos = state.items.map(x => ({...x}));
            productos.forEach(x => x.selected = false);
            console.log(productos);
            let fav: Producto = <Producto>productos.find(d => d.producto === (action as ElegidoFavoritoAction).producto.producto);
            fav.selected = true;
      
            return {
                ...state,
                items: <Producto[]>productos,
                favorito: fav
            };
        } 
        case ProductoActiontype.Vote_up: {
            const p: Producto = (action as VoteupAction).producto;
        p.voteUp();
            return { ...state };
        }

        case ProductoActiontype.Vote_down: {
            const p: Producto = (action as VoteDownAction).producto;
            p.voteDown;
                return { ...state };
            }
        case ProductoActiontype.INIT_MY_DATA: {
                const Productos: string[] = (action as InitMyDataAction).productos;
                return {
                    ...state,
                    items: Productos.map((d) => new Producto(d, '', 0))
                  };
              }

    }
return state;

}
//effects 
@Injectable()
export class ProductoEffects{
    @Effect()
    nuevoagregado: Observable<Action> = this.actions$.pipe(
        ofType(ProductoActiontype.Nuevo_Producto),
        map((action: NuevoProductoAction)=> new ElegidoFavoritoAction(action.producto))
    );
        constructor(private actions$: Actions){}
}
