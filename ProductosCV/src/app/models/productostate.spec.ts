import {
    ReducerProductos,
    productostate,
    intializeProducto,
    InitMyDataAction,
    NuevoProductoAction
  } from './productostate.model';
  import { Producto } from './Producto.model';
  
  describe('ReducerProductos', () => {
    it('should reduce init data', () => {
      const prevState: productostate = intializeProducto();
      const action: InitMyDataAction = new InitMyDataAction(['cobija 1', 'Sabana 2']);
      const newState: productostate =     ReducerProductos(prevState, action);
      expect(newState.items.length).toEqual(2);
      expect(newState.items[0].producto).toEqual('cobija 1');
    });
  
    it('should reduce new item added', () => {
      const prevState: productostate = intializeProducto();
      const action: NuevoProductoAction = new NuevoProductoAction(new Producto('cobijas', 'url',1));
      const newState: productostate = ReducerProductos(prevState, action);
      expect(newState.items.length).toEqual(1);
      expect(newState.items[0].producto).toEqual('cobijas');
    });
  });