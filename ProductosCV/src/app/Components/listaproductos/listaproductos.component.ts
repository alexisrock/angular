import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoProductoAction } from '../../models/productostate.model';
import { Producto } from '../../models/Producto.model';
import { Productoapicliente } from '../../models/Productoapicliente.model';

@Component({
  selector: 'app-listaproductos',
  templateUrl: './listaproductos.component.html',
  styleUrls: ['./listaproductos.component.css']
})
export class ListaproductosComponent implements OnInit {  
@Output() agregarItem: EventEmitter<Producto>;
update: string[];
All;

  constructor(public productoapicliente: Productoapicliente, public store: Store<AppState>) {
    this.agregarItem = new EventEmitter<Producto>();
    this.update = [];  
    this.store.select(state => state.producto.favorito).subscribe(data => { 
      if(data!= null){
        this.update.push("se a elegido a "+data.producto);
      } 
    });
    store.select(state=> state.producto.items).subscribe(items => this.All =items);
  }

  ngOnInit(): void {
    this.store.select(state => state.producto.favorito)
    .subscribe(data => {
      const f = data;
      if (f != null) {
        this.update.push('Se eligió: ' + f.producto );
      }
    });
  }

  agregadoproduct(p: Producto)  {

    this.productoapicliente.add(p);
    this.agregarItem.emit(p);
  //  this.store.dispatch(new NuevoProductoAction(p));
  }

  elegido(p: Producto){  
    this.productoapicliente.elegir(p);
 //   this.store.dispatch(new ElegidoFavoritoAction(p));
  }
  getAll(){
    

  }

}
