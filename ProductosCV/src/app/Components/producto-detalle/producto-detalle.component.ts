import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Producto } from '../../models/Producto.model';
import { Productoapicliente } from '../../models/Productoapicliente.model';

@Component({
  selector: 'app-producto-detalle',
  templateUrl: './producto-detalle.component.html',
  styleUrls: ['./producto-detalle.component.css'],
  providers:[Productoapicliente ]
})
export class ProductoDetalleComponent implements OnInit {
producto: Producto;
style = {
  sources: {
    world: {
      type: 'geojson',
      data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
    }
  },
  version: 8,
  layers: [{
    'id': 'countries',
    'type': 'fill',
    'source': 'world',
    'layout': {},
    'paint': {
      'fill-color': '#6F788A'
    }
  }]
};

constructor(private route: ActivatedRoute, private productoapicliente: Productoapicliente) {}



  ngOnInit(): void {
    let id =  this.route.snapshot.paramMap.get('id');
    this.producto = this.productoapicliente.getById(id);
  }





}
