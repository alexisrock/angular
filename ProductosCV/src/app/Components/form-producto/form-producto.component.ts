import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { stringify } from 'querystring';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Producto } from '../../models/Producto.model';
import { ajax, AjaxResponse } from 'rxjs/ajax';

import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-producto',
  templateUrl: './form-producto.component.html',
  styleUrls: ['./form-producto.component.css']
})
export class FormProductoComponent implements OnInit {
@Output() agregarItem: EventEmitter<Producto>;
fg: FormGroup;
minLongNombre= 5;
searchResults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { 
    this.agregarItem = new EventEmitter();
    this.fg = fb.group({
      producto: ['', Validators.compose([
        Validators.required,
        this.validarnombre,
        this.validadorparametrizable(this.minLongNombre)
      ])],
      url: ['']
    });
    this.fg.valueChanges.subscribe((form: any)=>{
      console.log('cambio en el formulario' +form);
    }   
    );
  }

  ngOnInit(): void {
    const elemNombre = <HTMLInputElement>document.getElementById('producto');
    fromEvent(elemNombre, 'input').pipe(
      map((e: KeyboardEvent)=> (e.target as HTMLInputElement).value),
      filter(text => text.length>2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndpoint+"/productos?q="+text)))
      .subscribe(AjaxResponse => this.searchResults = AjaxResponse.response);
  }
  guardar(producto: string, url: string): boolean{
    const d = new Producto(producto, url);
    this.agregarItem.emit(d);
    return false;
  }

  validarnombre(control:FormControl):{[s: string]:boolean} {
    const l = control.value.toString().trim().length;
    if(l>0 && l < 5){
      return {invalidNombre : true};
    }
    else{
      return null;
    }
  }

  validadorparametrizable(minlog: number): ValidatorFn{
    return (control: FormControl): {[s:string] : Boolean} | null =>  {
      const l = control.value.toString().trim().length;
      if(l>0 && l < minlog){
        return {minLongNombre : true};
      }
      else{
        return null;
      }
      return null; 
    }
  }


}
