import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { AppState } from '../../app.module';
import { Producto } from '../../models/Producto.model';
import { Store} from '@ngrx/store';
import { VoteDownAction, VoteupAction } from '../../models/productostate.model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-productoproueba',
  templateUrl: './productoproueba.component.html',
  styleUrls: ['./productoproueba.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})
export class ProductoprouebaComponent implements OnInit {
@Input()  producto: Producto;
@Input('idx')  posicion: number;
@HostBinding('attr.class') cssClass = 'col-md-4';
@Output() clicked: EventEmitter<Producto>;

constructor(public store: Store<AppState>) { 
  this.clicked = new EventEmitter();
  }

  ngOnInit() {
    
  }

  ir(){
    this.clicked.emit(this.producto);
    return false;
  }


  VoteUp(){
    this.store.dispatch(new VoteupAction(this.producto));
      return false;   
  }
  VoteDown(){
    this.store.dispatch(new VoteDownAction(this.producto));
    return false;
  }
}
