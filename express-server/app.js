var express = require("express"), cors = require('cors');
var app = express();
app.use(cors());
app.listen(3000,()=> console.log("Servidor run por el puerto 3000"));

var productos = ["sabanas", "Cobijas","toallas","cubrelechos","demas"];
app.get("/productos",(req, res,next)=>res.json(productos.filter((c)=> c.toLowerCase().indexOf(
    req.query.q.toString().toLowerCase()) > -1)));

var misProductos= [];
app.get("/my",(req, res, next)=> res.json(misProductos));
app.post("/my", (req, res, next)=>{
    console.log(req.body);
    misProductos.push(req.body);
    res.json(misProductos);
});

app.get("/api/translation", (req, res, next) => res.json([
    {lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang}
  ]));